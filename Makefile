# Makefile for Eric Raymond's CP/M-like hex dumper
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

hexd: hexd.c
	$(CC) -DRELEASE=\"$(VERS)\" -O hexd.c -o hexd

SOURCES = README COPYING NEWS.adoc control Makefile hexd.c hexd.adoc

hexd-$(VERS).tar.gz: $(SOURCES) hexd.1 
	@ls $(SOURCES) hexd.1 | sed s:^:hexd-$(VERS)/: >MANIFEST
	@(cd ..; ln -s hexd hexd-$(VERS))
	(cd ..; tar -czf hexd/hexd-$(VERS).tar.gz `cat hexd/MANIFEST`)
	@(cd ..; rm hexd-$(VERS))

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

install: hexd.1 uninstall
	cp hexd /usr/bin
	cp hexd.1 /usr/share/man/man1/hexd.1

uninstall:
	rm -f /usr/bin/hexd /usr/share/man/man1/hexd.

clean:
	rm -f hexd hexd-$(VERS).tar.gz *.rpm *.1 *.html MANIFEST SHIPPER.*

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

cppcheck:
	cppcheck --template gcc --enable=all hexd.c

check: hexd
	@./hexd hexd.png | diff -u hexd.chk -
	@echo "No diff output is good news."

dist: hexd-$(VERS).tar.gz

release: hexd-$(VERS).tar.gz hexd.html
	shipper version=$(VERS) | sh -e -x

refresh: hexd.html
	shipper -N -w version=$(VERS) | sh -e -x
